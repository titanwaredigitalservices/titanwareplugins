﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System;
using UnityEngine.UI;
public class FBRemoteConfigManager : MonoBehaviour
{

    /// <summary>
    /// Guide Links
    /// 
    /// Firebase
    /// https://console.firebase.google.com/u/0/
    /// 
    /// Firebase Unity Samples
    /// https://github.com/firebase/quickstart-unity 
    /// </summary>


    //   public Text test;

    public static FBRemoteConfigManager instance { get; private set; }

    public CrossPromotionInit _CrossPromotion;
    public bool LogParameterStatus;
    public bool IAPParameterEnabled;

    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;

    protected bool isFirebaseInitialized = false;

    public static bool FetchDone;

    //Test variables
    bool BoolTest;
    long IntTest;
    string StringTest;
    double FloatTest;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    // Initialize remote config, and set the default values.
    void InitializeFirebase()
    {
        System.Collections.Generic.Dictionary<string, object> defaults =
          new System.Collections.Generic.Dictionary<string, object>();

        // These are the values that are used if we haven't fetched data from the
        // server
        // yet, or if we ask for values that the server doesn't have:
        //defaults.Add("config_test_string", "default local string");
        //defaults.Add("config_test_int", 1);
        //defaults.Add("config_test_float", 1.0);
        //defaults.Add("config_test_bool", false);

        //defaults.Add("CrossPromotionImage", "default");
        //defaults.Add("CrossPromotionEnabled", false);
        //defaults.Add("CrossPromotionURL", "default");

        // Firebase.RemoteConfig.FirebaseRemoteConfig.SetDefaults(defaults);
        isFirebaseInitialized = true;

        //Fetch the data from the server
        FetchDataAsync();
    }

    // Display the currently loaded data.  If fetch has been called, this will be
    // the data fetched from the server.  Otherwise, it will be the defaults.
    // Note:  Firebase will cache this between sessions, so even if you haven't
    // called fetch yet, if it was called on a previous run of the program, you
    //  will still have data from the last time it was run.
    private void GetData()
    {
        ///EXAMPLES
        // IntTest = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("config_test_int").LongValue;
        //  FloatTest = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("config_test_float").DoubleValue;
        //StringTest = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("config_test_string").StringValue;
        //  BoolTest = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotionEnabled").BooleanValue; 

#if UNITY_ANDROID    

        CrossPromotionInit.CrossPromotionEnabled = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotionEnabled").BooleanValue;
        CrossPromotionInit.CrossPromotionImageURL = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotionImage").StringValue;
        CrossPromotionInit.CrossPromotionURL = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotionURL").StringValue;

        Rate.RateCountToShow = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ShowRateUsEvery").LongValue;
        ForceReviewStatic.ForceReview = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("forceReview").BooleanValue;
        ForceReviewStatic.forceReviewLan = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("forceReviewLan").StringValue;       

        if (IAPParameterEnabled)
        {
            // TitanwareIAP.showInApp = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("showInApp").BooleanValue;
            // TitanwareIAP.ShowInAppEvery = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ShowInAppEvery").LongValue;
            TitanwareIAP.freeTrialActive = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("freeTrialActive").BooleanValue;
            TitanwareIAP.subscriptionActive = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("subscriptionActive").BooleanValue;
            TitanwareIAP.subscriptionActiveES_PT = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("subscriptionActiveES_PT").BooleanValue;
        }

        TestAdShow.AdsEnabled = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AdsEnabled").BooleanValue;
        TestAdShow.AdsEnabledES_PT = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AdsEnabledES_PT").BooleanValue;
        TestAdShow.AdCountToShow = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ShowAdsEveryQuestion").LongValue;
        TestAdShow.EnableLoadingAd = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("EnableLoadingAd").BooleanValue;
        TestAdShow.showFbFirst = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("showFbFirst").BooleanValue;
        TestAdShow.AdsAtStartUp = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AdsAtStartUp").BooleanValue;


#endif
#if UNITY_IOS
        CrossPromotionInit.CrossPromotionEnabled = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotionEnablediOS").BooleanValue;
        CrossPromotionInit.CrossPromotionImageURL = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotionImageiOS").StringValue;
        CrossPromotionInit.CrossPromotionURL = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("CrossPromotionURLiOS").StringValue;

        Rate.RateCountToShow = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ShowRateUsEveryiOS").LongValue;
        ForceReviewStatic.ForceReview = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("forceReviewiOS").BooleanValue;
        ForceReviewStatic.forceReviewLan = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("forceReviewLaniOS").StringValue;

       
        if (IAPParameterEnabled)
        {
            // TitanwareIAP.showInApp = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("showInAppiOS").BooleanValue;
           // TitanwareIAP.ShowInAppEvery = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ShowInAppEveryiOS").LongValue;
            TitanwareIAP.freeTrialActive = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("freeTrialActiveiOS").BooleanValue;
            TitanwareIAP.subscriptionActive = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("subscriptionActiveiOS").BooleanValue;
            TitanwareIAP.subscriptionActiveES_PT = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("subscriptionActiveES_PTiOS").BooleanValue;
        }

        TestAdShow.AdsEnabled = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AdsEnablediOS").BooleanValue;
        TestAdShow.AdsEnabledES_PT = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AdsEnabledES_PTiOS").BooleanValue;
        TestAdShow.AdCountToShow = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("ShowAdsEveryQuestioniOS").LongValue;
        TestAdShow.EnableLoadingAd = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("EnableLoadingAdiOS").BooleanValue;
        TestAdShow.showFbFirst = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("showFbFirstiOS").BooleanValue;
        TestAdShow.AdsAtStartUp = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("AdsAtStartUpiOS").BooleanValue;
#endif
        FetchDone = true;

        LogParameters(LogParameterStatus);
    }

    // Start a fetch request.
    public Task FetchDataAsync()
    {
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number.  For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        System.Threading.Tasks.Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync(
            TimeSpan.Zero);
        return fetchTask.ContinueWith(FetchComplete);
    }

    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.Log("Fetch Canceled");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.Log("Fetch encountered an error");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.Log("Fetch completed successfully");
        }

        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus)
        {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                GetData();
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason)
                {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.Log("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.Log("Latest Fetch call still pending.");
                break;
        }
    }

    private void LogParameters(bool enabled)
    {
        if (enabled)
        {
            Debug.Log("AdsEnabledES_PT: " + TestAdShow.AdsEnabledES_PT);
            Debug.Log("subscriptionActiveES_PT: " + TitanwareIAP.subscriptionActiveES_PT);
            Debug.Log("ForceReview: " + ForceReviewStatic.ForceReview);
            Debug.Log("ForceReviewLan: " + ForceReviewStatic.forceReviewLan);
            Debug.Log("RateCountToShow: " + Rate.RateCountToShow);
            Debug.Log("AdsEnabled: " + TestAdShow.AdsEnabled);
            Debug.Log("AdCountToShow: " + TestAdShow.AdCountToShow);
            Debug.Log("EnableLoadingAd: " + TestAdShow.EnableLoadingAd);
            Debug.Log("ShowFbFirst:" + TestAdShow.showFbFirst);
            Debug.Log("AdsAtStartUp:" + TestAdShow.AdsAtStartUp);

            if (IAPParameterEnabled)
            {
                Debug.Log("showInApp: " + TitanwareIAP.showInApp);
                Debug.Log("ShowInAppEvery: " + TitanwareIAP.ShowInAppEvery);
                Debug.Log("FreeTrialActive: " + TitanwareIAP.freeTrialActive);
                Debug.Log("SubscriptionActive: " + TitanwareIAP.subscriptionActive);
            }

        }
    }

}
