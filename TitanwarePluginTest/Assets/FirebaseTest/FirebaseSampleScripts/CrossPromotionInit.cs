﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class CrossPromotionInit : MonoBehaviour {

    public static CrossPromotionInit instance { get; private set; }

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    // The output of the image
    public Image img;
    public Animator _imgAnimator;

    // The source URL
    public static string CrossPromotionURL;
    //The source Image URL
    public static string CrossPromotionImageURL;
    //Is cross promotion enabled
    public static bool CrossPromotionEnabled;

    private bool ImageLoaded;   

    public void OpenURL()
    {
        Application.OpenURL(CrossPromotionURL);
    }   

    public void LoadImage()
    {
        if (TestAdShow.instance.BlockAdsIAPEnabled)
        {
            if (TitanwareIAP.Subscribed)
                return;
        }

        if (!TestAdShow.CrossShown)
        {
            if (CrossPromotionEnabled)
            {
                if (ImageLoaded)
                {
                    OpenImage();
                }
                else
                {
                    StartCoroutine(SetImageTexture());
                }
            }
            else
            {
                Debug.Log("Cross promotion is disabled, Enable it inside the Firebase Remote Config");
            }
        }
    }

    public void CloseImage()
    {
        _imgAnimator.SetTrigger("scaledown");
    }

    IEnumerator SetImageTexture()
    {
        //Fetch an image from this URL
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(CrossPromotionImageURL);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture2D tex;
            tex = new Texture2D(4, 4, TextureFormat.DXT1, false);

            tex = DownloadHandlerTexture.GetContent(www);

            //Get the whole screen size of the texture
            Rect rec = new Rect(0, 0, tex.width, tex.height);

            //Create a sprite and assign the texture to the sprite
            Sprite s = Sprite.Create(tex, rec, new Vector2(0.5f, 0.5f), 100);

            //Apply the new sprite to the image sprite
            img.sprite = s;
            ImageLoaded = true;

            OpenImage();
        }
    }

    private void OpenImage()
    {
        TestAdShow.CrossShown = true;
        _imgAnimator.SetTrigger("scaleup");
    }
}
