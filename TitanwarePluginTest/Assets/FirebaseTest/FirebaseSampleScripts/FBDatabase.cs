﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using UnityEngine.UI;
using System.Net;
using System;
public class FBDatabase : MonoBehaviour
{
    /// <summary>
    /// SAMPLE USAGE     
    /// 
    /// FOR UPDATING THE TABLE
    /// 
    ///PushUpdateData(SampleUpdateChildrenDict());
    /// PushReplaceData(SampleValueAsyncDict());
    /// 
    /// https://firebase.google.com/docs/database/unity/start
    /// 
    /// https://firebase.google.com/docs/database/unity/save-data
    /// 
    /// https://firebase.google.com/docs/database/unity/retrieve-data     
    /// </summary>

    public static FBDatabase instance { get; private set; }

    private static DatabaseReference reference;

    public string FirebaseURL;

    public Text SampleData;
    public Text AnotherSampleData;

    bool connection;

    private void Awake()
    {
        if(instance ==null)
        {
            instance = this;
        }
    }


    public void requestTableInfo()
    {
        checkConnection();

        FirebaseDatabase.DefaultInstance
                        .GetReference("TableName")
        .GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("Loading Database Error");
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;

                //Get the child key of the Table
                SampleData.text = snapshot.Child("child1").Child("child2").Value.ToString();

                //Get the number of childs inside the table
                AnotherSampleData.text = snapshot.ChildrenCount.ToString();
            }
        });
    }


    public void PushUpdateData(Dictionary<string, object> DataDictionary)
    {
        checkConnection();

        reference.Child("TableName").Child("child1").UpdateChildrenAsync(DataDictionary);     
    }

    public void PushReplaceData(Dictionary<string,object> DataDictionary)
    {
        checkConnection();

        reference.Child("TableName").Child("child1").SetValueAsync(DataDictionary);
    }

    Dictionary<string, object> SampleUpdateChildrenDict()
    {
        ///Test this with the PushUpdateData
        
        Dictionary<string, object> childUpdates = new Dictionary<string, object>();
        Dictionary<string, object> ChildParent = new Dictionary<string, object>();

        childUpdates.Add("PlayerKill", "1");
        ChildParent.Add("PlayerProgress", childUpdates);

        return ChildParent;
    }    
    
    Dictionary<string, object> SampleValueAsyncDict()
    {
        ///Test this with the PushReplaceData
        ///Remove the current table and replace with these keys
        
        Dictionary<string, object> PlayerBoard = new Dictionary<string, object>();
        Dictionary<string, object> Player1 = new Dictionary<string, object>();
        Dictionary<string, object> Player1keys = new Dictionary<string, object>();

        Player1keys.Add("name", "user1");
        Player1keys.Add("ID", "1");
        Player1keys.Add("score", "10");

        Player1.Add("player1", Player1keys);

        PlayerBoard.Add("PlayerList", Player1);

        return PlayerBoard;
    }


    private void checkConnection()
    {
        if (connection == false)
        {
            // Get the root reference location of the database.
            reference = FirebaseDatabase.DefaultInstance.RootReference;
            connection = true;
        }

    }


}
