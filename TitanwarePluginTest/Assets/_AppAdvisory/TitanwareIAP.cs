﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class TitanwareIAP : MonoBehaviour
{
    /// <summary>
    /// If you are going to use this script, Implement the Unity IAP plugin and insert the Purchase.cs inside the IAPCanvas as a component together with this script
    /// </summary>
    public static TitanwareIAP instance { get; private set; }

    public static bool showInApp; //Check if the InApp UI will show and this is linked into the Remote Config of Firebase
    public static bool Subscribed;

    public static long ShowInAppEvery;
    public static int ShowInAppCount;
    public static bool freeTrialActive;
    public static bool subscriptionActive;
    public static bool subscriptionActiveES_PT;
    public static bool IAPisOpen;
    public static bool FirstShowInAppDone;

    public bool FeatureEnabled;
    [Space(10)]

    [Header("Old IAP")]
    public GameObject OldIAPUI;
    public Text OldIAPPriceTxt;
    [Space(10)]

    public GameObject InAppUI;
    public GameObject InBetweenUI;
    public GameObject SubscriptionLoadedUI;
    public GameObject LoadingUI;
    public GameObject RestorePurchaseUI;

    public Text MonthlyPriceTxt;
    public Text InBetweenPriceTxt;

    public string trialprice;
    public string monthlyprice;
    public string RemoveAdsPrice;

    [Header("Free Trial and Weekly")]
    public GameObject FreeTrialTitle;
    public GameObject WeeklyTitle;

    public Text TrialPriceTxt;
    public Text WeeklyPriceTxt;

    [Header("Privacy Policy")]
    public Text PrivacyTxt;
    public Text InBetweenPrivacyTxt;
    private string PrivacyMessage;

    [Header("IAP Buttons")]
    public Image FreeTrialImg;
    [Header("Free Trial In-Active")]
    public Sprite FreeTrialUnselected;
    public Sprite FreeTrialSelected;
    [Space(10)]
    [Header("Free Trial Active")]
    public Sprite FreeTrialActiveUnselected;
    public Sprite FreeTrialActiveSelected;
    [Space(10)]
    public Image MonthlyImg;
    public Sprite MontlyUnselected;
    public Sprite MonthlySelected;

    public string selectedIAP;

    private float starteradsTimer;


    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        if (!FeatureEnabled)
            return;

        InAppUI.SetActive(false);
        LoadingUI.SetActive(true);
        SubscriptionLoadedUI.SetActive(false);
        OldIAPUI.SetActive(false);
        RestorePurchaseUI.SetActive(false);

        LoadSubscriptionData();

        SelectFreeTrial();
    }

    private void Update()
    {
        if (FBRemoteConfigManager.FetchDone)
        {
            starteradsTimer += Time.deltaTime;

            if (starteradsTimer >= 10)
            {
                LoadingUI.SetActive(false);
                FBRemoteConfigManager.FetchDone = false;
            }
        }

        SetPriceText();
    }

    public void ShowInAppAtStart()
    {
        if (freeTrialActive)
        {
            WeeklyTitle.SetActive(false);
            FreeTrialTitle.SetActive(true);

            TrialPriceTxt.gameObject.SetActive(true);
            WeeklyPriceTxt.gameObject.SetActive(false);

            InBetweenPriceTxt.text = TrialPriceTxt.text;
            FreeTrialImg.sprite = FreeTrialActiveSelected;

        }
        else
        {
            WeeklyTitle.SetActive(true);
            FreeTrialTitle.SetActive(false);

            TrialPriceTxt.gameObject.SetActive(false);
            WeeklyPriceTxt.gameObject.SetActive(true);

            InBetweenPriceTxt.text = WeeklyPriceTxt.text;
            FreeTrialImg.sprite = FreeTrialSelected;
        }

        FirstShowInAppDone = true;
        OpenInAppUI();
    }


    public void StartPurchaseAdsBlocker()
    {
        LoadingUI.SetActive(true);

        StartCoroutine(InitiatePurchaseAdsBlock());
    }

    IEnumerator InitiatePurchaseAdsBlock()
    {
        yield return new WaitForSeconds(1);
        Purchaser.instance.BuyNonConsumable();
#if UNITY_ANDROID
        yield return new WaitForSeconds(3);
#elif UNITY_IOS
                yield return new WaitForSeconds(5);
#endif
        LoadingUI.SetActive(false);
    }

    public void GetBlockAdsPrice(string price)
    {
        RemoveAdsPrice = price;
        OldIAPPriceTxt.text = price;
    }

    public void OpenRestorePurchaseUI()
    {
        RestorePurchaseUI.SetActive(true);
    }

    /// <summary>
    /// Put this inside the Restore button
    /// </summary>
    public void RestorePurchase()
    {
        Purchaser.instance.RestorePurchases();
    }

    public void ShowIAPStarterAds()
    {
        TestAdShow.instance.ShowStarterAds();
        CloseInAppUI();
    }

    public void ShowIAPInsterstitial()
    {
        TestAdShow.instance.ShowInsterstitial();
        CloseInBetweenAppUI();
    }

    public void StartInBetweenSubscribe()
    {
        LoadingUI.SetActive(true);
        selectedIAP = "freetrial";

        StartCoroutine(InitiateSubcription());
    }

    public void SelectFreeTrial()
    {
        MonthlyImg.sprite = MontlyUnselected;

        if (freeTrialActive)
        {
            FreeTrialImg.sprite = FreeTrialActiveSelected;
        }
        else
        {
            FreeTrialImg.sprite = FreeTrialSelected;
        }

        selectedIAP = "freetrial";
    }

    public void SelectMonthly()
    {
        MonthlyImg.sprite = MonthlySelected;

        if (freeTrialActive)
        {
            FreeTrialImg.sprite = FreeTrialActiveUnselected;
        }
        else
        {
            FreeTrialImg.sprite = FreeTrialUnselected;
        }

        selectedIAP = "monthly";
    }

    public void StartSubscribe()
    {
        LoadingUI.SetActive(true);

        StartCoroutine(InitiateSubcription());
    }

    IEnumerator InitiateSubcription()
    {
        yield return new WaitForSeconds(1);
        if (selectedIAP == "freetrial")
        {
            Purchaser.instance.BuySubscription();
        }
        else if (selectedIAP == "monthly")
        {
            Purchaser.instance.BuyMonthlySubscription();
        }
#if UNITY_ANDROID
        yield return new WaitForSeconds(3);
#elif UNITY_IOS
                yield return new WaitForSeconds(5);
#endif
        LoadingUI.SetActive(false);
    }

    public void OpenInAppUI()
    {
        if (ForceReviewStatic.ForceReview)
        {
            if (LanguageManager.LANGUAGE_NAME == ForceReviewStatic.forceReviewLan)
            {
                if (ForceReviewManager.instance.GameLocked && !Subscribed)
                {
                    ForceReviewManager.instance.OpenForceReviewUI();
                }
                else
                {
                    OpenSubscriptionIAP();
                }
            }
            else
            {
                OpenSubscriptionIAP();
            }
        }
        else
        {
            OpenSubscriptionIAP();
        }
    }

    public void OpenSubscriptionIAP()
    {
        if (LanguageManager.LANGUAGE_NAME == "ES" || LanguageManager.LANGUAGE_NAME == "PT")
        {
            if (subscriptionActiveES_PT)
            {
                if (!Subscribed)
                {
                    IAPisOpen = true;
                    InAppUI.SetActive(true);
                    SelectFreeTrial();

                    UnityAdsManager.instance.HideBanner();
                }
            }
            else
            {
                OpenOldIAP();
            }
        }
        else
        {
            if (subscriptionActive)
            {
                if (!Subscribed)
                {
                    IAPisOpen = true;
                    InAppUI.SetActive(true);
                    SelectFreeTrial();

                    UnityAdsManager.instance.HideBanner();
                }
            }
            else
            {
                OpenOldIAP();
            }
        }
    }

    public void OpenOldIAP()
    {
        if (!Subscribed)
        {
            IAPisOpen = true;
            OldIAPUI.SetActive(true);
        }
    }

    public void CloseInAppUI()
    {
        IAPisOpen = false;

        InAppUI.SetActive(false);
        OldIAPUI.SetActive(false);

        TestAdShow.instance.Requestbanner();
    }

    public void OpenInBetweenAppUI()
    {
        if (!Subscribed)
        {
            IAPisOpen = true;
            InBetweenUI.SetActive(true);
            UnityAdsManager.instance.HideBanner();
        }
    }

    public void CloseInBetweenAppUI()
    {
        IAPisOpen = false;
        InBetweenUI.SetActive(false);
    }

    public void GetIAPPrice(string trial, string monthly)
    {
        trialprice = trial;
        monthlyprice = monthly;

        SetPrvacyPolicyTxt();
    }

    public void SetPriceText()
    {
        OldIAPPriceTxt.text = RemoveAdsPrice;

        if (LanguageManager.LANGUAGE_NAME == "EN")
        { //ENGLISH
            if (freeTrialActive)
            {
                TrialPriceTxt.text = "Free for 3 days, then " + trialprice + " per week";
            }
            else
            {
                WeeklyPriceTxt.text = trialprice + " per week";
            }
            MonthlyPriceTxt.text = monthlyprice + " per month";
        }

        if (LanguageManager.LANGUAGE_NAME == "ES") //SPANISH
        {
            if (freeTrialActive)
            {
                TrialPriceTxt.text = "Gratis por 3 días, después " + trialprice + " por semana";
            }
            else
            {
                WeeklyPriceTxt.text = trialprice + " por semana";
            }


            MonthlyPriceTxt.text = monthlyprice + " por mes";
        }

        if (LanguageManager.LANGUAGE_NAME == "PT") //PORTOGUESE
        {
            if (freeTrialActive)
            {
                TrialPriceTxt.text = "Livre, durante 3 dias, em seguida " + trialprice + " por semana";
            }
            else
            {
                WeeklyPriceTxt.text = trialprice + " por semana";
            }

            MonthlyPriceTxt.text = monthlyprice + " por mês";

        }

        if (LanguageManager.LANGUAGE_NAME == "DE") //GERMAN
        {
            if (freeTrialActive)
            {
                TrialPriceTxt.text = "3 Tage lang gratis, danach " + trialprice + " wöchentlich";
            }
            else
            {
                WeeklyPriceTxt.text = trialprice + " pro Woche";
            }

            MonthlyPriceTxt.text = monthlyprice + " pro Monat";
        }

        if (LanguageManager.LANGUAGE_NAME == "FR") //FRENCH
        {
            if (freeTrialActive)
            {
                TrialPriceTxt.text = "Gratuit 3 jours, puis " + trialprice + " hebdomadaire";
            }
            else
            {
                WeeklyPriceTxt.text = trialprice + " par semaine";
            }

            MonthlyPriceTxt.text = monthlyprice + " par mois";

        }

        if (LanguageManager.LANGUAGE_NAME == "IT") //ITALIAN
        {
            if (freeTrialActive)
            {
                TrialPriceTxt.text = "Gratis per 3 giorni, poi " + trialprice + " a settimana";
            }
            else
            {
                WeeklyPriceTxt.text = trialprice + " a settimana";
            }

            MonthlyPriceTxt.text = monthlyprice + " al mese";
        }

        if (freeTrialActive)
        {
            InBetweenPriceTxt.text = TrialPriceTxt.text;
        }
        else
        {
            InBetweenPriceTxt.text = WeeklyPriceTxt.text;
        }
    }

    public void SetPrvacyPolicyTxt()
    {
        PrivacyMessage = "Premium version offers weekly subscription for " + trialprice + " after 3 - days free trial or monthly subscription for " +
            monthlyprice + " Payment will be charged to Itunes Account at confirmation of purchase.Subscription automatically renews unless auto - renew is turned off at least 24 - hours before the end of the current period.The account will be charged for renewal within 24 - hours prior to the end of the current period at.the cost of the chosen package.Subscriptions may be managed by the user and auto - renewal may be turned off by going to the user's Account Settings after purchase. " +
            "Any unused portion of a free trial period will be forfeited when the user purchases a subscription to that publication, where applicable";

        PrivacyTxt.text = PrivacyMessage;
        InBetweenPrivacyTxt.text = PrivacyMessage;
    }

    public void OpenWebpage(string url)
    {
        Application.OpenURL(url);
    }

    private void LoadSubscriptionData()
    {
        if (PlayerPrefs.HasKey("subscribe"))
        {
            Subscribed = true;
        }
        else
        {
            Subscribed = false;
        }
    }

    public void ActivateSubscribersFeatures()
    {
        InAppUI.SetActive(false);
        OldIAPUI.SetActive(false);

        PlayerPrefs.SetString("subscribe", "subscribe");
        PlayerPrefs.Save();
        Subscribed = true;
        IAPisOpen = false;

        StartCoroutine(ReloadGame());
    }

    public void LoadSubscribersFeatures()
    {
        InAppUI.SetActive(false);
        OldIAPUI.SetActive(false);

        Subscribed = true;

        if (!PlayerPrefs.HasKey("subscribe"))
        {
            PlayerPrefs.SetString("subscribe", "subscribe");
            PlayerPrefs.Save();
            SubscriptionLoadedUI.SetActive(true);
            StartCoroutine(ReloadGame());
        }
    }

    IEnumerator ReloadGame()
    {
        yield return new WaitForSeconds(2);
        SubscriptionLoadedUI.SetActive(false);
        LoadingUI.SetActive(false);
        SceneManager.LoadScene(0);
    }

}
