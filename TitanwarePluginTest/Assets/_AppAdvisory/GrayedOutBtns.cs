﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GrayedOutBtns : MonoBehaviour, IPointerClickHandler
{
    public Color lockedColor;
    public Color UnlockedColor;
    public GameObject LockedImage;
    private Image spriterenderer;
    private Button btn;

    void Start()
    {
        spriterenderer = GetComponent<Image>();
        btn = GetComponent<Button>();
    }

    private void Update()
    {
        UpdateColor();
    }

    private void UpdateColor()
    {
        if (ForceReviewStatic.ForceReview)
        {
            if (LanguageManager.LANGUAGE_NAME == ForceReviewStatic.forceReviewLan)
            {
                if (TitanwareIAP.Subscribed || !ForceReviewManager.instance.GameLocked)
                {
                    LockedImage.SetActive(false);

                    spriterenderer.color = UnlockedColor;
                    btn.interactable = true;
                }
                else
                {
                    LockedImage.SetActive(true);
                    spriterenderer.color = lockedColor;
                    btn.interactable = false;
                }
            }
            else
            {

                if (!TitanwareIAP.Subscribed)
                {
                    LockedImage.SetActive(true);
                    spriterenderer.color = lockedColor;
                    btn.interactable = false;
                }
                else
                {
                    LockedImage.SetActive(false);
                    spriterenderer.color = UnlockedColor;
                    btn.interactable = true;
                }
            }
        }
        else
        {

            if (!TitanwareIAP.Subscribed)
            {
                LockedImage.SetActive(true);
                spriterenderer.color = lockedColor;
                btn.interactable = false;
            }
            else
            {
                LockedImage.SetActive(false);
                spriterenderer.color = UnlockedColor;
                btn.interactable = true;
            }
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {

        if (ForceReviewStatic.ForceReview)
        {
            if (LanguageManager.LANGUAGE_NAME == ForceReviewStatic.forceReviewLan)
            {
                if (TitanwareIAP.Subscribed || !ForceReviewManager.instance.GameLocked)
                {
                    LockedImage.SetActive(false);

                    spriterenderer.color = UnlockedColor;
                    btn.interactable = true;
                }
                else
                {
                    TitanwareIAP.instance.OpenInAppUI();
                }
            }
            else
            {

                if (!TitanwareIAP.Subscribed)
                {
                    TitanwareIAP.instance.OpenInAppUI();

                }
                else
                {
                    LockedImage.SetActive(false);
                    spriterenderer.color = UnlockedColor;
                    btn.interactable = true;
                }
            }
        }
        else
        {

            if (!TitanwareIAP.Subscribed)
            {
                TitanwareIAP.instance.OpenInAppUI();

            }
            else
            {
                LockedImage.SetActive(false);
                spriterenderer.color = UnlockedColor;
                btn.interactable = true;
            }
        }
    }


}
