﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class ForceReviewManager : MonoBehaviour
{
    public static ForceReviewManager instance { get; private set; }
    public GameObject ForceReviewRate;

    public bool GameLocked;

    public int StarsAmount;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        if (PlayerPrefs.HasKey("GameLocked"))
        {
            GameLocked = false;
        }
        else
        {
            GameLocked = true;
        }
    }

    private void Start()
    {
        ForceReviewRate.SetActive(false);
    }

    public void OpenForceReviewUI()
    {
        ForceReviewRate.SetActive(true);
    }

    public void CloseForceReviewUI()
    {
        ForceReviewRate.SetActive(false);
    }

    //This is added inside the ForceReviewRate UI button
    public void OpenRateForceReview()
    {
#if UNITY_ANDROID
        UnlockGame();

        if (StarsAmount >= 7)
        {
            Rate.OpenRateSystem();
        }
        else
        {
            ForceReviewRate.SetActive(false);
        }

#elif UNITY_IOS
        StartCoroutine(UnlockGameDelay());
        iOSReviewRequest.Request();
#endif 
    }

    public void UnlockGame()
    {
        if (ForceReviewStatic.ForceReview && GameLocked)
        {
            ForceReviewRate.SetActive(false);

            GameLocked = false;

            PlayerPrefs.SetString("GameLocked", "unlocked");
            PlayerPrefs.Save();

            SceneManager.LoadScene(0);
        }
    }

    IEnumerator UnlockGameDelay()
    {
        yield return new WaitForSeconds(2);
        UnlockGame();
    }
}
