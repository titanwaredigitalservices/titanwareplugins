﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class TitanwareMenu : MonoBehaviour
{    

    public void CallOpenRestore()
    {
        TitanwareIAP.instance.OpenRestorePurchaseUI();
    }

    public void OpenIAP()
    {
        TitanwareIAP.instance.OpenSubscriptionIAP();
    }

    public void ChangeScene(int sceneNum)
    {
        StartCoroutine(DelayChangeScene(sceneNum));
    }

    IEnumerator DelayChangeScene(int sceneNum)
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneNum);
    }

    public void OpenRate()
    {
        Rate.OpenRateUI();
    }

    public void LoadCrossPromotion()
    {
        CrossPromotionInit.instance.LoadImage();
    }
}
