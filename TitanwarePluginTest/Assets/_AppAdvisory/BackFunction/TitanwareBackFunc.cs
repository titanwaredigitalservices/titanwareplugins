﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TitanwareBackFunc : MonoBehaviour
{
    public GameObject QuitGameUI;
    public string GameplaySceneName;

    [Header("Enable this if you want to return to last scene after pressing back button")]
    [Space(5)]
    public bool ReturnToLastScene;

    Scene scene;

    void Start()
    {
        QuitGameUI.SetActive(false);
        scene = scene = SceneManager.GetActiveScene();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!TitanwareIAP.IAPisOpen && TitanwareIAP.FirstShowInAppDone)
            {
                if (ReturnToLastScene)
                {
                    SceneManager.LoadScene(scene.buildIndex - 1);
                }
                else
                {
                    QuitGameUI.SetActive(true);
                }
            }
        }
    }   

    public void MoveToPlayScene()
    {
        SceneManager.LoadScene(GameplaySceneName);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// This method could be used for IOS when you created a UI back button
    /// </summary>
    public void BackFunction()
    {
        if (!TitanwareIAP.IAPisOpen)
        {
            if (ReturnToLastScene)
            {
                SceneManager.LoadScene(scene.buildIndex - 1);
            }
            else
            {
                QuitGameUI.SetActive(true);
            }
        }
    }
}
