﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//#if UNITY_ANDROID
//using Google.Play.Review;
//#endif

public class Rate : MonoBehaviour
{
    public static int RateCounter = 0;
    public static long RateCountToShow = 0;

    public static int starsamount;

    public GameObject OfficialRateSystem;

    private bool alreadyRated;

//#if UNITY_ANDROID
//    private ReviewManager _reviewManager;
//    private PlayReviewInfo _playReviewInfo;
//#endif

    static Rate self;

    private void Start()
    {
        self = this;
        OfficialRateSystem.SetActive(false);

        if (PlayerPrefs.HasKey("rated"))
        {
            alreadyRated = true;
        }
        else
        {
            alreadyRated = false;
        }
        
//#if UNITY_ANDROID
//        _reviewManager = new ReviewManager();
//        StartCoroutine(StartRequestReviewFlow());
//#endif
    }

    void Update()
    {
        if (RateCountToShow >= 1)
        {
            if (RateCounter >= RateCountToShow)
            {
                if (!alreadyRated)
                {
                    RateCounter = 0;
                    OpenRateUI();
                }
            }
        }
    }

    public static void OpenRateUI()
    {
        self.OfficialRateSystem.SetActive(true);
    }

    public void CloseRateUI()
    {
        OfficialRateSystem.SetActive(false);
    }

    // put inside Rate UI Stars buttons  
    public void ActivateRateNative()
    {
#if UNITY_ANDROID
        if (starsamount >= 6)
        {
            starsamount = 0;
            OpenRateSystem();
            OfficialRateSystem.SetActive(false);
        }
        else
        {
            starsamount = 0;
            OfficialRateSystem.SetActive(false);
        }
#elif UNITY_IOS
        OpenRateSystem();
        OfficialRateSystem.SetActive(false);
#endif

        ActivateAlreadyRated();
    }

    private void ActivateAlreadyRated()
    {
        alreadyRated = true;
        PlayerPrefs.SetString("rated", "rated");
        PlayerPrefs.Save();

    }

    public static void OpenRateSystem()
    {
#if UNITY_ANDROID
        // self.OfficialRateSystem.SetActive(true);
        //self.OpenAppReview();
        string identifier = Application.identifier;
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + identifier);
#elif UNITY_IOS
        iOSReviewRequest.Request();
#endif
    }


//#if UNITY_ANDROID

//    private void OpenAppReview()
//    {
//        StartCoroutine(OpenReviewFlow());
//    }

//    IEnumerator StartRequestReviewFlow()
//    {
//        var requestFlowOperation = _reviewManager.RequestReviewFlow();
//        yield return requestFlowOperation;
//        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
//        {
//            Debug.Log("*****RATE ERROR: " + requestFlowOperation.Error.ToString() + "*******");
//            yield break;
//        }
//        _playReviewInfo = requestFlowOperation.GetResult();
//    }

//    IEnumerator OpenReviewFlow()
//    {
//        if (_playReviewInfo == null)
//        {
//            var requestFlowOperation = _reviewManager.RequestReviewFlow();

//            yield return requestFlowOperation;
//            if (requestFlowOperation.Error != ReviewErrorCode.NoError)
//            {
//                Debug.Log("*****RATE ERROR: " + requestFlowOperation.Error.ToString() + "*******");
//                yield break;
//            }
//            _playReviewInfo = requestFlowOperation.GetResult();
//        }

//        yield return new WaitForSeconds(1);

//        var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);

//        yield return launchFlowOperation;

//        _playReviewInfo = null; // Reset the object

//        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
//        {
//            Debug.Log("*****RATE ERROR: " + launchFlowOperation.Error.ToString() + "*******");
//            yield break;
//        }
//    }
//#endif

}
