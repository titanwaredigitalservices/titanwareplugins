﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LanguageChanger : MonoBehaviour
{
    public GameObject LanguageUI;

    private void Start()
    {
        LanguageUI.SetActive(true);
    }

    public void SetLanguage(string langname)
    {
        LanguageManager.instance.SetLanguage(langname);

        CloseLanguageSelection();
    }

    public void CloseLanguageSelection()
    {
        LanguageUI.SetActive(false);

        TitanwareIAP.instance.ShowInAppAtStart();
    }

}
