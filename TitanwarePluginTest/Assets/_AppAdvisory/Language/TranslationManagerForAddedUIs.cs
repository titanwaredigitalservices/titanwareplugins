﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslationManagerForAddedUIs : MonoBehaviour
{
    public Text myText;

    public string[] translations;

    void Update()
    {

        if (LanguageManager.LANGUAGE_NAME == "EN")
        { //ENGLISH
            myText.text = translations[0];
        }

        if (LanguageManager.LANGUAGE_NAME == "ES") //SPANISH
        {
            myText.text = translations[1];
        }

        if (LanguageManager.LANGUAGE_NAME == "PT") //PORTOGUESE
        {
            myText.text = translations[2];
        }

        if (LanguageManager.LANGUAGE_NAME == "DE") //GERMAN
        {
            myText.text = translations[3];
        }

        if (LanguageManager.LANGUAGE_NAME == "FR") //FRENCH
        {
            myText.text = translations[4];
        }

        if (LanguageManager.LANGUAGE_NAME == "IT") //ITALIAN
        {
            myText.text = translations[5];
        }

    }
}
