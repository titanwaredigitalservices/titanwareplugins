﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LanguageManager : MonoBehaviour
{
    public static LanguageManager instance { get; private set; }

    public static string LANGUAGE_NAME;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        CheckSystemLanguage();
    }

    private void CheckSystemLanguage()
    {
        if (Application.systemLanguage == SystemLanguage.English)
        {
            SetLanguage("English");
        }
        if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            SetLanguage("Spanish");
        }
        if (Application.systemLanguage == SystemLanguage.Portuguese)
        {
            SetLanguage("Portuguese");
        }
        if (Application.systemLanguage == SystemLanguage.German)
        {
            SetLanguage("German");
        }
        if (Application.systemLanguage == SystemLanguage.French)
        {
            SetLanguage("French");
        }
        if (Application.systemLanguage == SystemLanguage.Italian)
        {
            SetLanguage("Italian");
        }
    }

    public void SetLanguage(string langname)
    {
        switch (langname)
        {
            case "English":
                LANGUAGE_NAME = "EN";
                break;
            case "Spanish":
                LANGUAGE_NAME = "ES";
                break;
            case "Portuguese":
                LANGUAGE_NAME = "PT";
                break;
            case "German":
                LANGUAGE_NAME = "DE";
                break;
            case "French":
                LANGUAGE_NAME = "FR";
                break;
            case "Italian":
                LANGUAGE_NAME = "IT";
                break;
        }

        TitanwareIAP.instance.SetPrvacyPolicyTxt();
    }
}
