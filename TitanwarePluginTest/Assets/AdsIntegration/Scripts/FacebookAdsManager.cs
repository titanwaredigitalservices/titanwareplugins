﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudienceNetwork;
using UnityEngine.UI;
public class FacebookAdsManager : MonoBehaviour
{
    public static FacebookAdsManager instance { get; private set; }

    public string IOSInsterstitialPlacement;
    public string AndroidInsterstitialPlacement;
    public string BannerPlacement;
    public string RewardedPlacement;

    private bool didClose;

    /// <summary>
    /// Insterstitial
    /// </summary>
    private InterstitialAd interstitialAd;
    private bool InsterstitialIsLoaded;

    /// <summary>
    /// Banner
    /// </summary>
    private AdView adView;

    /// <summary>
    /// Rewarded Ad
    /// </summary>
    private RewardedVideoAd rewardedVideoAd;
    private bool RewardedIsLoaded;

    /// <summary>
    /// Properties
    /// </summary>
    public bool GetInsterstitiaIslLoaded { get { return InsterstitialIsLoaded; } }
    public bool GetRewardedIsLoaded { get { return RewardedIsLoaded; } }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        AudienceNetworkAds.Initialize();
    }

    public void RequestInsterstitial()
    {
#if !UNITY_EDITOR

        // Create the interstitial unit with a placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
#if UNITY_ANDROID

        interstitialAd = new InterstitialAd(AndroidInsterstitialPlacement);
#endif
#if UNITY_IOS
        interstitialAd = new InterstitialAd(IOSInsterstitialPlacement);
#endif

        interstitialAd.Register(this.gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        this.interstitialAd.InterstitialAdDidLoad = (delegate () {
            Debug.Log("Interstitial ad loaded.");
            this.InsterstitialIsLoaded = true;
            didClose = false;

        });
        interstitialAd.InterstitialAdDidFailWithError = (delegate (string error) {
            Debug.Log("Interstitial ad failed to load with error: " + error);
        });
        interstitialAd.InterstitialAdWillLogImpression = (delegate () {
            Debug.Log("Interstitial ad logged impression.");
        });
        interstitialAd.InterstitialAdDidClick = (delegate () {
            Debug.Log("Interstitial ad clicked.");
        });

        this.interstitialAd.interstitialAdDidClose = (delegate () {
            Debug.Log("Interstitial ad did close.");
            didClose = true;
            if (this.interstitialAd != null)
            {
                this.interstitialAd.Dispose();
            }
        });

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Interstitial activity has
         * been destroyed without being properly closed. This can happen if an
         * app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        interstitialAd.interstitialAdActivityDestroyed = delegate () {
            if (!didClose)
            {
                Debug.Log("Interstitial activity destroyed without being closed first.");
                Debug.Log("Game should resume.");
            }
        };
#endif

        // Initiate the request to load the ad.
        interstitialAd.LoadAd();
#endif

    }
    public void ShowInsterstitial()
    {
#if !UNITY_EDITOR
        if (InsterstitialIsLoaded)
        {
            interstitialAd.Show();
            InsterstitialIsLoaded = false;
            StartCoroutine(requestDelay());
        }
        else if (!InsterstitialIsLoaded)
        {
            TestAdShow.instance.ShowInsterstitial();
        }
#endif
    }

    IEnumerator requestDelay()
    {
        yield return new WaitForSeconds(3);
        TestAdShow.instance.RequestInsterstitial();
    }

    public void LoadBanner(AdPosition AdPosition)
    {
        if (adView)
        {
            adView.Dispose();
        }

        // Create a banner's ad view with a unique placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
        adView = new AdView(BannerPlacement, AdSize.BANNER_HEIGHT_50);

        adView.Register(gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        adView.AdViewDidLoad = delegate () {
            adView.Show(AdPosition);
        };

        adView.AdViewDidFailWithError = (delegate (string error) {
            Debug.Log("Banner failed to load with error: " + error);
            TestAdShow.instance.Requestbanner();//show admob ads
        });
        adView.AdViewWillLogImpression = (delegate () {
            Debug.Log("Banner logged impression.");
        });

        // Initiate a request to load an ad.
        adView.LoadAd();
    }

    public void RequestRewardedVideo()
    {
        // Create the rewarded video unit with a placement ID (generate your own on the Facebook app settings).
        // Use different ID for each ad placement in your app.
        this.rewardedVideoAd = new RewardedVideoAd(RewardedPlacement);

        this.rewardedVideoAd.Register(this.gameObject);

        // Set delegates to get notified on changes or when the user interacts with the ad.
        this.rewardedVideoAd.RewardedVideoAdDidLoad = (delegate () {
            Debug.Log("RewardedVideo ad loaded.");
            this.RewardedIsLoaded = true;
            didClose = false;
        });
        this.rewardedVideoAd.RewardedVideoAdDidFailWithError = (delegate (string error) {
            Debug.Log("RewardedVideo ad failed to load with error: " + error);
        });
        this.rewardedVideoAd.RewardedVideoAdWillLogImpression = (delegate () {
            Debug.Log("RewardedVideo ad logged impression.");
        });
        this.rewardedVideoAd.RewardedVideoAdDidClick = (delegate () {
            Debug.Log("RewardedVideo ad clicked.");
        });

        this.rewardedVideoAd.RewardedVideoAdDidClose = (delegate () {
            Debug.Log("Rewarded video ad did close.");
            didClose = true;
            if (this.rewardedVideoAd != null)
            {
                this.rewardedVideoAd.Dispose();
            }
        });

#if UNITY_ANDROID
        /*
         * Only relevant to Android.
         * This callback will only be triggered if the Rewarded Video activity
         * has been destroyed without being properly closed. This can happen if
         * an app with launchMode:singleTask (such as a Unity game) goes to
         * background and is then relaunched by tapping the icon.
         */
        rewardedVideoAd.RewardedVideoAdActivityDestroyed = delegate ()
        {
            if (!didClose)
            {
                Debug.Log("Rewarded video activity destroyed without being closed first.");
                Debug.Log("Game should resume. User should not get a reward.");
            }
        };
#endif
        // Initiate the request to load the ad.
        this.rewardedVideoAd.LoadAd();
    }


    public void ShowRewardedVideo()
    {
        if (RewardedIsLoaded)
        {
            rewardedVideoAd.Show();
            RewardedIsLoaded = false;
        }      
    }

    void OnDestroy()
    {
        // Dispose of interstitial ad when the scene is destroyed
        if (interstitialAd != null)
        {
            interstitialAd.Dispose();
        }

        // Dispose of rewardedVideo ad when the scene is destroyed
        if (rewardedVideoAd != null)
        {
            rewardedVideoAd.Dispose();
        }
    }


}
