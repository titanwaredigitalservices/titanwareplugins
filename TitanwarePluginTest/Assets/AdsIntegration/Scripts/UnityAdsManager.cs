﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
public class UnityAdsManager : MonoBehaviour, IUnityAdsListener
{
    public static UnityAdsManager instance { get; private set; }

	//Insert you Unity Ads ID here
#if UNITY_IOS
	 string gameID = "1234567";
#elif UNITY_ANDROID
	string gameID = "1234567";
#endif

	public bool TestMode;
	public bool EnableBanner;

	private string VideoPlacement = "video";
	private string RewardedPlacement = "rewardedVideo";
	private string BannerPlacement = "banner";

	void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

	void Start()
	{
		Advertisement.AddListener(this);

		Advertisement.Initialize(gameID, TestMode);
	}

	public void ShowUnityVidAd()
	{
		Advertisement.Show(VideoPlacement);
	}

	public void ShowUnityRewardedAd()
	{
		Advertisement.Show(RewardedPlacement);
	}		

	public bool IsUnityVidAvailable()
	{
		if (Advertisement.IsReady(VideoPlacement))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public bool IsUnityRewardedAvailable()
	{
		if (Advertisement.IsReady(RewardedPlacement))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// Implement IUnityAdsListener interface methods:
	public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
	{
		// Define conditional logic for each ad completion status:
		if (showResult == ShowResult.Finished)
		{
			if(placementId == RewardedPlacement)
			{
				//give reward
			}

			Advertisement.Initialize(gameID, TestMode);			
		}
		else if (showResult == ShowResult.Skipped)
		{
			Advertisement.Initialize(gameID, TestMode);
		}
		else if (showResult == ShowResult.Failed)
		{
			Advertisement.Initialize(gameID, TestMode);
		}
	}

	public void OnUnityAdsDidError(string message)
	{
		// Log the error.
		Debug.Log("**UNITY ERROR: " + message);
	}

	public void OnUnityAdsDidStart(string placementId)
	{
		// Optional actions to take when the end-users triggers an ad.
	}

	public void OnUnityAdsReady(string placementId)
	{
		// If the ready Placement is rewarded, show the ad:
		//if (placementId == myPlacementId)
		//{
		//	Advertisement.Show(myPlacementId);
		//}
	}

	IEnumerator WaitForAd()
	{
		float currentTimeScale = Time.timeScale;
		Time.timeScale = 0f;
		yield return null;

		while (Advertisement.isShowing)
			yield return null;

		Time.timeScale = currentTimeScale;
	}

	//If you want to show banner in your game, Call the TestAdShow.instance.requestbanner instead
	public void ShowBanner()
	{
		if (!EnableBanner)
			return;

		StartCoroutine(ShowBannerWhenInitialized());
	}

	IEnumerator ShowBannerWhenInitialized()
	{
		while (!Advertisement.isInitialized)
		{
			yield return new WaitForSeconds(0.5f);
		}

		Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
		Advertisement.Banner.Show(BannerPlacement);
	}

	public void HideBanner()
	{
		if (!EnableBanner)
			return;

		Advertisement.Banner.Hide();
	}


}
