﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using AudienceNetwork;
using UnityEngine.SceneManagement;
public class TestAdShow : MonoBehaviour
{
    public static TestAdShow instance { get; private set; }  

    [Space(15)]
    public bool BlockAdsIAPEnabled;

    /// <summary>
    /// Static variables for Firebase Remote Config
    /// </summary>
    public static bool AdsEnabled;
    public static bool AdsEnabledES_PT;

    public static int AdCounter = 0;
    public static long AdCountToShow;

    public static bool EnableLoadingAd;
    public static bool showFbFirst;
    public static bool AdsAtStartUp;

    public GameObject LoadAdObject;

    public static bool CrossShown;

    public static bool starteradsclicked;

    float timer;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void Start()
    {
        if (LoadAdObject != null)
        {
            LoadAdObject.SetActive(false);
        }

        RequestInsterstitial();
    }

    private void Update()
    {
        if (AdsEnabled)
        {
            AdTimerLoad();
        }

        if (AdCountToShow >= 1)
        {
            if (AdCounter >= AdCountToShow)
            {
                AdCounter = 0;

                if (LanguageManager.LANGUAGE_NAME == "ES" || LanguageManager.LANGUAGE_NAME == "PT")
                {
                    if (AdsEnabledES_PT)
                    {
                        if (TitanwareIAP.subscriptionActiveES_PT)
                        {
                            TitanwareIAP.instance.OpenInBetweenAppUI();
                        }
                        else
                        {
                            ShowInsterstitial();
                        }
                    }
                }
                else
                {
                    if (AdsEnabled)
                    {
                        if (TitanwareIAP.subscriptionActive)
                        {
                            TitanwareIAP.instance.OpenInBetweenAppUI();
                        }
                        else
                        {
                            ShowInsterstitial();
                        }
                    }
                }
            }
        }
    }

    public void Requestbanner()
    {
        if (LanguageManager.LANGUAGE_NAME == "ES" || LanguageManager.LANGUAGE_NAME == "PT")
        {
            if (AdsEnabledES_PT)
            {
                if (BlockAdsIAPEnabled)
                {
                    if (TitanwareIAP.Subscribed)
                        return;
                }
                UnityAdsManager.instance.ShowBanner();
            }
        }
        else
        {
            if (AdsEnabled)
            {
                if (BlockAdsIAPEnabled)
                {
                    if (TitanwareIAP.Subscribed)
                        return;
                }
                UnityAdsManager.instance.ShowBanner();
            }
        }
    }
 
    private void AdTimerLoad()
    {
        timer += Time.deltaTime;

        if (timer >= 60)
        {
            if (!FacebookAdsManager.instance.GetInsterstitiaIslLoaded)
            {
                timer = 0;
                RequestInsterstitial();
            }
        }
    }

    public void ShowStarterAds()
    {
        if (!starteradsclicked)
        {
            if (LanguageManager.LANGUAGE_NAME == "ES" || LanguageManager.LANGUAGE_NAME == "PT")
            {
                if (AdsEnabledES_PT)
                {
                    ShowSetInsterstitial();
                }
            }
            else
            {
                if (AdsEnabled)
                {

                    ShowSetInsterstitial();
                }
            }
        }
    }
  

    public void RequestInsterstitial()
    {
        if (BlockAdsIAPEnabled)
        {
            if (TitanwareIAP.Subscribed)
                return;
        }

        StartCoroutine(RequestAdsCoroutine());

    }

    IEnumerator RequestAdsCoroutine()
    {
        yield return new WaitForSeconds(2);      
        FacebookAdsManager.instance.RequestInsterstitial();
    }
  
    public void ShowInsterstitial()
    {
        if (BlockAdsIAPEnabled)
        {
            if (TitanwareIAP.Subscribed)
                return;
        }
        if (LanguageManager.LANGUAGE_NAME == "ES" || LanguageManager.LANGUAGE_NAME == "PT")
        {
            if (AdsEnabledES_PT)
            {
                ShowTheInsterstitial();
            }
        }
        else
        {
            if (AdsEnabled)
            {
                ShowTheInsterstitial();
            }
        }
    }

    private void ShowTheInsterstitial()
    {
        if (!FacebookAdsManager.instance.GetInsterstitiaIslLoaded)
        {
            RequestInsterstitial();
        }

        if (UnityAdsManager.instance.IsUnityVidAvailable() || FacebookAdsManager.instance.GetInsterstitiaIslLoaded)
        {
            if (LanguageManager.LANGUAGE_NAME == "ES" || LanguageManager.LANGUAGE_NAME == "PT")
            {
                if (TitanwareIAP.subscriptionActiveES_PT)
                {
                    ShowSetInsterstitial();
                }
                else
                {
                    if (EnableLoadingAd)
                    {
                        if (LoadAdObject != null)
                        {
                            LoadAdObject.SetActive(true);
                            StartCoroutine(CloseAdObject());
                        }
                    }
                    else
                    {
                        ShowSetInsterstitial();
                    }
                }
            }
            else
            {
                if (TitanwareIAP.subscriptionActive)
                {
                    ShowSetInsterstitial();
                }
                else
                {
                    if (EnableLoadingAd)
                    {
                        if (LoadAdObject != null)
                        {
                            LoadAdObject.SetActive(true);
                            StartCoroutine(CloseAdObject());
                        }
                    }
                    else
                    {
                        ShowSetInsterstitial();
                    }
                }
            }
        }
    }

    IEnumerator CloseAdObject()
    {
        yield return new WaitForSeconds(2);
        LoadAdObject.SetActive(false);
        ShowSetInsterstitial();
    }

    private void ShowSetInsterstitial()
    {
        if (showFbFirst)//Show fb ad first
        {
            if (FacebookAdsManager.instance.GetInsterstitiaIslLoaded)
            {
                FacebookAdsManager.instance.ShowInsterstitial();
                StartCoroutine(requestDelay());
            }
            else if (UnityAdsManager.instance.IsUnityVidAvailable())
            {
                UnityAdsManager.instance.ShowUnityVidAd();
            }
            else
            {
                RequestInsterstitial();
            }
        }
        else //Show unityads first
        {
            if (UnityAdsManager.instance.IsUnityVidAvailable())
            {
                UnityAdsManager.instance.ShowUnityVidAd();
            }
            else if (FacebookAdsManager.instance.GetInsterstitiaIslLoaded)
            {
                FacebookAdsManager.instance.ShowInsterstitial();
                StartCoroutine(requestDelay());
            }
            else
            {
                RequestInsterstitial();
            }
        }
    }

    IEnumerator requestDelay()
    {
        yield return new WaitForSeconds(3);
        RequestInsterstitial();
    }

    public void ShowRewardedVid()
    {     

        if (LanguageManager.LANGUAGE_NAME == "ES" || LanguageManager.LANGUAGE_NAME == "PT")
        {
            if (AdsEnabledES_PT)
            {
                UnityAdsManager.instance.ShowUnityRewardedAd();
            }
        }
        else
        {
            if (AdsEnabled)
            {
                UnityAdsManager.instance.ShowUnityRewardedAd();
            }
        }
    }



}




